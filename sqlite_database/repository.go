package sqlite_database

import (
	"database/sql"
	"errors"

	"reflect"
)

var (
	ErrDuplicate    = errors.New("record already exists")
	ErrNotExists    = errors.New("row not exists")
	ErrUpdateFailed = errors.New("update failed")
	ErrDeleteFailed = errors.New("delete failed")
)

type SQLiteDatabase struct {
	db *sql.DB
}

func NewSQLiteRepository(db *sql.DB) *SQLiteDatabase {
	return &SQLiteDatabase{
		db: db,
	}
}

// type Repository interface {
// 	Migrate() error
// 	Create(people People) (*People, error)
// 	All() ([]People, error)
// 	GetByID(id int64) (*People, error)
// 	Update(id int64, updated People) (*People, error)
// 	Delete(id int64) error
// }

func parse(myData interface{}) []interface{} {
	/*
		places the values of interfaces/structs into an array

		Code From : https://stackoverflow.com/a/27141801/18258698
	*/
	dataValue := reflect.ValueOf(myData)

	dataArray := make([]interface{}, dataValue.NumField()-1)

	/* skip over the 0th field, which is ID field*/
	for i := 0; i < dataValue.NumField()-1; i++ {
		switch dataValue.Field(i + 1).Kind() {
		case reflect.Struct:
			dataArray[i] = parse(dataValue.Field(i + 1).Interface())
		default:
			dataArray[i] = dataValue.Field(i + 1).Interface()
		}
	}
	return dataArray
}
