/*

SPECIAL THANKS TO : https://gosamples.dev/sqlite-intro/ (official documentation)
*/
package sqlite_database

import (
	"database/sql"
	"errors"
	"fmt"

	"reflect"

	"github.com/mattn/go-sqlite3"
)

// type Repository interface {
// 	Migrate() error
// 	Create(people People) (*People, error)
// 	All() ([]People, error)
// 	GetByID(id int64) (*People, error)
// 	Update(id int64, updated People) (*People, error)
// 	Delete(id int64) error
// }

/*
Define the SQLiteRepository struct and its constructor
Note that it requires an instance of sql.DB type as a dependency.
*/
// type SQLiteRepository struct {
// }

type People_SQLiteDatabase struct {
	db *sql.DB
}

func New_People_SQLiteDatabase(db *sql.DB) *People_SQLiteDatabase {
	return &People_SQLiteDatabase{
		db: db,
	}
}

type PeopleRepository interface {
	Migrate() error
}

type People struct {
	ID        int64
	Firstname string
	Lastname  string
	Email     string `json:"email" validate:"required,email"`
}

// func NewUser(newPerson *People) *People {
// 	return &People{
// 		ID:        ai.ID(),
// 		Firstname: newPerson.Firstname,
// 		Lastname:  newPerson.Lastname,
// 		Email:     newPerson.Email,
// 	}
// }

/*
CREATE TABLE Logic
*/
func (database *People_SQLiteDatabase) Migrate() error {

	// USER database

	query_people := `
    CREATE TABLE IF NOT EXISTS people(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        firstname TEXT NOT NULL,
        lastname TEXT NOT NULL,
        email TEXT NOT NULL
    );

    `
	// PREDICTION database

	if _, err_people := database.db.Exec(query_people); err_people != nil { /* FAILED CREATION */
		return err_people

	} else { /* PASS CREATION */
		return err_people

	}
}

/*
CREATE NEW logic
*/

func (database *People_SQLiteDatabase) Create(people People) (*People, error) {

	// myData := yourData{5, "a", innerData{"b"}}
	argument_values := parse(people)

	SQL_QUERY := `INSERT INTO people(`

	fields := reflect.TypeOf(people)
	num := fields.NumField()

	for i := 0; i < num; i++ {
		field := fields.Field(i)
		if field.Name != "ID" {
			SQL_QUERY += field.Name
			if i < num-1 {
				SQL_QUERY += ","
			} else {
				break
			}

		}
	}

	SQL_QUERY += ") values ("

	for i := 0; i < num; i++ {
		SQL_QUERY += "?"
		if i < num-2 {
			SQL_QUERY += ","
		} else {
			break
		}
	}
	SQL_QUERY += ")"

	res, err := database.db.Exec(SQL_QUERY, argument_values...)

	if err != nil {
		var sqliteErr sqlite3.Error
		if errors.As(err, &sqliteErr) {
			if errors.Is(sqliteErr.ExtendedCode, sqlite3.ErrConstraintUnique) {
				return nil, ErrDuplicate
			}
		}
		return nil, err
	}

	if _, err := res.LastInsertId(); err != nil {
		return nil, err
	}
	// people.ID = id

	return &people, nil
}

func (database *People_SQLiteDatabase) All() ([]People, error) {
	rows, err := database.db.Query("SELECT * FROM people")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var all []People
	for rows.Next() {
		var people People
		if err := rows.Scan(&people.ID, &people.Firstname, &people.Lastname, &people.Email); err != nil {
			return nil, err
		}
		all = append(all, people)
	}
	return all, nil
}

func (database *People_SQLiteDatabase) GetById(id int64) (*People, error) {
	var people People

	SQL_QUERY := `SELECT * FROM people WHERE id = ?`

	/*
		For some reason, I can't dynamically code the Scan() code.
		I was thinking of doing
			database.db.QueryRow(SQL_QUERY, id).Scan(parse(&people)...)

		But I get this error
			reflect.flag.mustBe(...)
					/usr/lib/go-1.18/src/reflect/value.go:223
			reflect.Value.NumField({0x8266e0?, 0xc000109350?, 0x9359e0?})
					/usr/lib/go-1.18/src/reflect/value.go:1891 +0x8d
			example.com/sqlite_database.parse({0x8266e0?, 0xc000109350?})
			example.com/sqlite_database.(*People_SQLiteDatabase).GetById(0xc00012fc70, 0x89c4f4?)
			main.database_function()
			main.main()
			exit status 2

		If anyone knows how to fix this, please let me know
	*/
	if err := database.db.QueryRow(SQL_QUERY, id).Scan(&people.ID, &people.Firstname, &people.Lastname, &people.Email); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNotExists
		}
		// handle error
		return nil, err
	}
	return &people, nil

}

func (database *People_SQLiteDatabase) GetByEmail(email string) (*People, error) {
	var people People

	SQL_QUERY := `SELECT * FROM people WHERE email = ?`

	if err := database.db.QueryRow(SQL_QUERY, email).Scan(&people.ID, &people.Firstname, &people.Lastname, &people.Email); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNotExists
		}
		// handle error
		return nil, err
	}
	return &people, nil

}

/*
UPDATE Logic
*/

func (database *People_SQLiteDatabase) Update(id int64, updated People) (*People, error) {
	update_people := parse(updated)
	update_people = append(update_people, id)

	if id == 0 {
		return nil, errors.New("invalid updated ID")
	}

	SQL_QUERY := `UPDATE people SET `

	fields := reflect.TypeOf(updated)
	num := fields.NumField()

	for i := 0; i < num; i++ {

		field := fields.Field(i)
		// ignore ID field, DO NOT update ID field
		if field.Name != "ID" {
			SQL_QUERY += field.Name
			SQL_QUERY += " = ?"
			if i < num-1 {
				SQL_QUERY += ","
			} else {
				break
			}

		}
	}
	SQL_QUERY += `WHERE id = ?`
	/* SQL_QUERY =: `UPDATE people SET value = ?, value = ?, value = ? WHERE id = ?` */
	fmt.Println(SQL_QUERY)
	fmt.Println(update_people)

	res, err := database.db.Exec(SQL_QUERY, update_people...)
	if err != nil {
		return nil, err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return nil, err
	}

	if rowsAffected == 0 {
		return nil, ErrUpdateFailed
	}

	return &updated, nil
}

/*
DELETE Logic
*/

func (database *People_SQLiteDatabase) DeleteAll() error {
	res, err := database.db.Exec("DELETE FROM people")
	if err != nil {
		return err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDeleteFailed
	}

	return err
}

func (database *People_SQLiteDatabase) DeleteById(id int64) error {
	res, err := database.db.Exec("DELETE FROM people WHERE id = ?", id)
	if err != nil {
		return err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDeleteFailed
	}

	return err
}
