/*

SPECIAL THANKS TO : https://gosamples.dev/sqlite-intro/ (official documentation)
*/
package sqlite_database

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"

	"github.com/mattn/go-sqlite3"
)

/*
Define the SQLiteDatabase struct and its constructor
Note that it requires an instance of sql.DB type as a dependency.
*/

type History_SQLiteDatabase struct {
	db *sql.DB
}

func New_History_SQLiteDatabase(db *sql.DB) *History_SQLiteDatabase {
	return &History_SQLiteDatabase{
		db: db,
	}
}

/*

History struct

follow the columns present in the dataset

sepal_l | sepal_w | petal_l | petal_w



*/

type History struct {
	ID      int64
	Sepal_l float32 `json:"sepal_l" validate:"required, gt=0, lte=10"`
	Sepal_w float32 `json:"sepal_w" validate:"required, gt=0, lte=10"`
	Petal_l float32 `json:"petal_l" validate:"required, gt=0, lte=10"`
	Petal_w float32 `json:"petal_w" validate:"required, gt=0, lte=10"`
	Target  uint8   `json:"target" validate:"required, oneof=0 1 2 "`
}

func NewEntry(newEntry *History) *History {
	return &History{
		Sepal_l: newEntry.Sepal_l,
		Sepal_w: newEntry.Sepal_w,
		Petal_l: newEntry.Petal_l,
		Petal_w: newEntry.Petal_w,
		Target:  newEntry.Target,
	}
}

/*
CREATE TABLE Logic
*/
func (r *History_SQLiteDatabase) Migrate() error {

	/*
		History table:
			age | sex | bmi | children | smoker | region | charges

	*/

	query_History := `
    CREATE TABLE IF NOT EXISTS history(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        sepal_l REAL NOT NULL,
        sepal_w REAL NOT NULL,
        petal_l REAL NOT NULL,
        petal_w REAL NOT NULL,
		target INTEGER NOT NULL
    );

    `
	// PREDICTION database

	_, err_History := r.db.Exec(query_History)

	if err_History != nil { /*FAILED CREATION*/
		fmt.Println("FAILED CREATION - History TABLE")
	} else { /*PASSED CREATION*/
		fmt.Println("PASSED CREATION - History TABLE")
	}
	return err_History
}

/*READ ALL LOGIC*/

func (database *History_SQLiteDatabase) All() ([]History, error) {
	rows, err := database.db.Query("SELECT * FROM history")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var all []History
	for rows.Next() {
		var history_features History
		if err := rows.Scan(
			&history_features.ID,
			&history_features.Sepal_l,
			&history_features.Sepal_w,
			&history_features.Petal_l,
			&history_features.Petal_w,
			&history_features.Target,
		); err != nil {
			return nil, err
		}
		all = append(all, history_features)
	}
	return all, nil
}

/*
CREATE NEW logic
*/

func (database *History_SQLiteDatabase) Create(history History) (*History, error) {

	// myData := yourData{5, "a", innerData{"b"}}
	argument_values := parse(history)

	SQL_QUERY := `INSERT INTO history(`

	fields := reflect.TypeOf(history)
	num := fields.NumField()

	for i := 0; i < num; i++ {
		field := fields.Field(i)
		// dont insert id, already auto incrementing on SQL side
		if field.Name != "ID" {
			SQL_QUERY += field.Name
			if i < num-1 {
				SQL_QUERY += ","
			} else {
				break
			}

		}
	}

	SQL_QUERY += ") values ("

	for i := 0; i < num; i++ {
		SQL_QUERY += "?"
		if i < num-2 {
			SQL_QUERY += ","
		} else {
			break
		}
	}
	SQL_QUERY += ")"

	res, err := database.db.Exec(SQL_QUERY, argument_values...)

	if err != nil {
		var sqliteErr sqlite3.Error
		if errors.As(err, &sqliteErr) {
			if errors.Is(sqliteErr.ExtendedCode, sqlite3.ErrConstraintUnique) {
				return nil, ErrDuplicate
			}
		}
		return nil, err
	}

	if _, err := res.LastInsertId(); err != nil {
		return nil, err
	}
	// history.ID = id

	return &history, nil
}
