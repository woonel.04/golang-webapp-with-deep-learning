package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"example.com/sqlite_database"
	// "github.com/gosamples-dev/samples/sqlite-intro/people"
	// "example.com/people"
	_ "github.com/mattn/go-sqlite3"
)

const fileName = "./sqlite_database/joyboy.db"

func main() {
	os.Remove(fileName)

	db, err := sql.Open("sqlite3", fileName)
	if err != nil {
		log.Fatal(err)
	}

	// INITIALISING the SQLite repository
	peopleRepository := sqlite_database.New_People_SQLiteDatabase(db)

	if err := peopleRepository.Migrate(); err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}

	person1 := sqlite_database.People{

		Firstname: "Trafalgar",
		Lastname:  "Law",
	}

	person2 := sqlite_database.People{
		Firstname: "Roronoa",
		Lastname:  "Zoro",
	}

	person3 := sqlite_database.People{
		Firstname: "Black Leg",
		Lastname:  "Sanji",
	}

	created1, err := peopleRepository.Create(person1)
	if err != nil {
		log.Fatal(err)
	}

	/*
		CREATED person 2
	*/

	peopleRepository.Create(person2)
	if err != nil {
		log.Fatal(err)
	}

	created3, err := peopleRepository.Create(person3)
	if err != nil {
		log.Fatal(err)
	}

	gotById_1, err := peopleRepository.GetById(1)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("get by name: %+v\n", gotById_1)

	created3.Firstname = "Wings of Pirate King"
	if _, err := peopleRepository.Update(created3.ID, *created3); err != nil {
		log.Fatal(err)
	}

	all, err := peopleRepository.All()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("\nAll sqlite_databases:\n")
	for _, sqlite_database := range all {
		fmt.Printf("sqlite_database: %+v\n", sqlite_database)
	}

	if err := peopleRepository.DeleteById(created1.ID); err != nil {
		log.Fatal(err)
	}

	all, err = peopleRepository.All()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("\nAll sqlite_databases:\n")
	for _, sqlite_database := range all {
		fmt.Printf("sqlite_database: %+v\n", sqlite_database)
	}
}
