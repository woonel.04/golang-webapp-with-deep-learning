module example.com

go 1.18

require (
	github.com/asafschers/goscore v0.0.0-20190823112107-6e6e174c153a // indirect
	github.com/dmitryikh/leaves v0.0.0-20210121075304-82771f84c313 // indirect
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator/v10 v10.11.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/magic003/alice v0.0.0-20170426062316-2087f8381114 // indirect
	github.com/mattn/go-shellwords v1.0.12 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	github.com/pa-m/sklearn v0.0.0-20200711083454-beb861ee48b1 // indirect
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // indirect
	golang.org/x/sys v0.0.0-20210806184541-e5e7981a1069 // indirect
	golang.org/x/text v0.3.7 // indirect
)
